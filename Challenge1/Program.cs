﻿/* Challenge:
*
* Write a program to work out the areas of a rectangle.
*
* - Collect the width and height of the rectangle from
*   the keyboard
* - Calculate the area
* - Display the result
*
* Extension
* Display the volume of a cuboid
*/

using System;

namespace Challenge1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] shapeDimensions = {        // initialize array
                UserInteraction("Enter width: "),       // data entry for shape dimensions
                UserInteraction("Enter height: "),
                UserInteraction("Enter depth (0 if this is a 2D shape): ")
            };

            double answer;
            if (shapeDimensions[2] != 0) {      // if it's a cuboid
                answer = CalcArea(shapeDimensions[0], shapeDimensions[1], shapeDimensions[2]);  // calculate the volume
                Console.WriteLine("Volume of cuboid: {0} cm\u00b3", answer);      // write the volume
            } else {
                answer = CalcArea(shapeDimensions[0], shapeDimensions[1]);      // calculate the area
                Console.WriteLine("Area of rectangle: {0} cm\u00b2", answer);   // write the area
            }
            Console.ReadKey();      // pause console output
        }

        static double UserInteraction(string prompt)
        {
            double providedvalue;       // number the user will provide
            bool isnumber;      // whether the user has provided a valid number

            do {
                Console.Write(prompt);      // write the specified prompt
                isnumber = double.TryParse(Console.ReadLine(), out providedvalue);      // try parsing the user input
                if (!isnumber) {        // if it's not a number
                    Console.WriteLine("That's not a number");       // write error message
                }
            } while (!isnumber);        // keep going until there's a valid number provided
            return providedvalue;       // return the number
        }

        // calculate the area of a rectangle
        static double CalcArea(double width, double height)
        {
            return width * height;
        }

        // override: calculate the area of a cuboid
        static double CalcArea(double width, double height, double depth)
        {
            return width * height * depth;
        }
    }
}