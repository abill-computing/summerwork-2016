﻿/* Challenge:
*
* Write a program for a higher / lower guessing game
*
* The computer randomly generates a sequence of up to
* 10 numbers between 1 and 13. The player each after
* seeing each number in turn has to decide whether the
* next number is higher or lower.
*
* If you can remember Brucie's 'Play your cards right'
* it's basically that.
*
* If you get 10 guesses right you win the game.
*
* Extension:
* - Give the players two lives
* - Make sure only H or L can be entered
* 
* ---------------------------------------------------------------
* 
* Note: I have adapted this challenge - in its original 
* form (as shown above) there was the possibility that
* the randomly-generated next number could be the same
* as the current number.
* 
* Instead, I have made the game work as follows:
* - 12 numbers are selected from a deck containing the numbers 1-13
* - This array of 12 numbers is shuffled
* - The player is given the starting number
* - The player has to decide whether the next number in the array
*   is higher or lower than the current number
* 
* In my opinion, this works better, as the game can now be played
* tactically - if the current number is 13, the next number must
* be lower; if the current number is 3, the odds are that the next
* number will be higher, etc.
* 
*/

using System;
using System.Linq;

namespace Challenge2
{
    class Program
    {
        static int playerLives = 2;     // number of lives the player has
        static int playerPoints;        // number of points the player has
        const int WinThreshold = 10;    // the number of points required for a player to win
        static int[] numberSequence;    // the sequence of numbers ('cards') the game is playing with

        static void Main(string[] args)
        {
            Init();     // generate the deck
            Run();      // start the game
        }

        static void Init()
        {
            numberSequence = GenerateDeck(12, 13);      // generate a deck with 12 numbers between 1 and 13
            Console.WriteLine("Starting number: {0}", numberSequence[0]);   // write the starting number to the console
        }

        static void Game(int nextnum)
        {
            Console.Write("Higher (H) or lower (L)? ");     // write a prompt to the console
            var answer = Console.ReadKey();
            switch (char.ToLower(answer.KeyChar)) {     // read the character the player inputs
                case 'h':       // if the player thinks the next number is higher
                    if (numberSequence[nextnum] > numberSequence[nextnum - 1])      // if it is higher
                        Correct();      // they're right
                    else
                        Incorrect();    // they're not
                    break;

                case 'l':       // if the player thinks the next number is lower
                    if (numberSequence[nextnum] < numberSequence[nextnum - 1])      // if it is lower
                        Correct();      // they're right
                    else
                        Incorrect();    // they're not
                    break;

                default:        // if we get something we're not expecting
                    Console.WriteLine(" That's not an answer!");        // write error message
                    Game(nextnum);      // try again
                    break;
            }
        }

        static int[] GenerateDeck(int count, int max)
        {
            var randNum = new Random();     // initialize random number generator
            int[] sequence = Enumerable.Range(1, max).ToArray();        // Generate an array of numbers 1 to 'max'

            for (int i = 0; i < sequence.Length; i++) {     // Shuffle the numbers

                // Random position that hasn't already been shuffled
                int randpos = i + randNum.Next(sequence.Length - i);

                // Swap sequence[i] with the item at the position generated above
                int temp = sequence[randpos];
                sequence[randpos] = sequence[i];
                sequence[i] = temp;
            }
            return sequence.Take(count).ToArray();      // return the first 'count' values                                     
        }

        static void Run()
        {
            for (int nextnum = 1; nextnum < numberSequence.Length; nextnum++) {     // as long as we have numbers
                Game(nextnum);      // prompt for higher or lower
                Console.WriteLine("Next number: " + numberSequence[nextnum]);       // write the next number to console
            }
        }

        static void Incorrect()
        {
            playerLives--;      // remove a life
            if (playerLives == 0) {     // if player runs out of lives
                Console.WriteLine(" Incorrect. No lives remaining, sorry!");        // write message
                Environment.Exit(0);        // exit program
            } else {
                Console.WriteLine(" Incorrect, sorry. {0} lives remaining.", playerLives);      // write message
            }
        }

        static void Correct()
        {
            playerPoints++;     // add a point
            Console.WriteLine(" Correct! Points: {0}\n", playerPoints);     // write message
            if (playerPoints == WinThreshold)       // win if the player has enough points
                Win();
        }

        static void Win()
        {
            Console.Write("You won!");      // write message
            Environment.Exit(0);        // exit
        }
    }
}
